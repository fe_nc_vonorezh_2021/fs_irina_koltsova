const gulp = require('gulp'),
    concat = require('gulp-concat'), 
    uglify = require('gulp-uglify');


gulp.task('gulp-uglify', function(){
    gulp.src('../task_4/*/*.js')
    .pipe(concat('index.js')) // объединение всех найденных файлов .js в один файл index.js
    .pipe(uglify()) // сжимаем
    .pipe(gulp.dest('./dist/')) //закидываем в папку dist
});