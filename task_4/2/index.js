let name = prompt('Введите Ваше имя', '');
let age = prompt('Введите Ваш возраст', '');
if (age < 0 || age == null || isNaN(age)) {
    age = prompt('Введите корректный возраст', '');
}
//Альтернативный вариант ввода возраста
//Вводим возраст до тех пор, пока значение не станет соответствующим
/*do {
    age = prompt('Введите Ваш возраст', '');
} while (age <= 0 || age == null || isNaN(age));*/
name = name[0].toUpperCase() + name.substring(1); //Заглавная буква
alert('Привет, ' + name + ' тебе уже ' + age + ' лет!');