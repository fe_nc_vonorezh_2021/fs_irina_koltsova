let arr = Array.from({length: 40}, () => Math.floor(Math.random() * 40)); //длина массива выбрана 40
document.write('<p>Исходный массив: ' + arr + '</p>');
//Сортировка массива по возрастанию
function ascending(a, b) {
	return a - b;
}
let ascArr = arr.sort(ascending);
document.write('<p>Массив по возрастанию: ' + ascArr + '</p>');

//Сортировка по убыванию
function descending(a, b) {
	return b - a;
}
let descArr = arr.sort(descending);
document.write('<p>Массив по убыванию: ' + descArr + '</p>');

//Вторая часть задания
/*let count = 0;
function sum() {
	for (var i = 0; i < arr.length; i++) {
		if (arr[i] % 2 != 0) {
			count += Math.pow(arr[i], 2);			  
		}
	}
	return count;
}*/

let summ = arr.reduce((sum, count) => sum + (count%2 != 0 ? count*count : 0), 0);
//let summ = (arr) => arr.reduce(sum => sum + Math.pow((arr.filter(item => item % 2 != 0)), 2), 0); //начудила и решила еще попробовать через map

//let sum = 0;
//let summ = arr.map(item => {if (item % 2 !== 0) sum += Math.pow(item, 2)}); //через map тоже работает

document.write('<p>Сумма квадратов нечетных чисел равна: ' + summ + '</p>');

