let a = 10, b = 15;
console.log('Условие a = ' + a + ' b = ' + b);
let c = b;
b = a;
a = c;
console.log('Используя третью переменную, поменяем значения a и b: а = ' + a + ' b = ' + b);
console.log('Условие: a = ' + a + ' b = ' + b);
[a, b] = [b, a];
console.log('Используя лишь две переменные a и b получим: а = ' + a + ' b = ' + b);


