let table = document.querySelector('table'),
    popupBG = document.querySelector('.popup__bg'),
    popup = document.querySelector('.popup'), 
    popupClose = document.querySelector('.popup__close'),    
    textArea = document.querySelector('.popup__message'),
    button = [...document.querySelectorAll('button')],
    input = [...document.querySelectorAll('input')],
    text;

//действие с таблицей
//выбираем ячейки для заполнения td
//text - выбранная ячейка
table.addEventListener ('click', (e) => {
    e.preventDefault();
    let target = e.target.closest('td');
    if(!table.contains(target)) return;
      
    popupBG.classList.add('active');
    popup.classList.add('active');

    textArea.focus();
    text = target;
    textArea.value = text.innerHTML;        
});

//закрытие модального окна
popupClose.addEventListener('click', () => {        
    textArea.value = '';
    popupBG.classList.remove('active');
    popup.classList.remove('active');
})

//закрытие модального окна при клике вне окна
window.onclick = (event) => {
    if (event.target == popupBG) {
        popupBG.classList.remove('active');
        popup.classList.remove('active');
    }
}

//действие кнопок: добавить, редактировать, удалить
button.forEach((elem) => {
    elem.addEventListener('click', () => {
        if (elem.className == 'popup__add') { 
            (text.innerHTML.length > 0) ? textArea.value = text.innerHTML : text.innerHTML = textArea.value;     
        } else if (elem.className == 'popup__edit') {
            text.innerHTML = textArea.value;
        } else if (elem.className == 'popup__delete') {
            text.innerHTML = '';
            textArea.value = '';
        }                   
    })
})

//добавить и удалить строку
input.forEach((elem) => {
    elem.addEventListener('click', () => {
        if (elem.className == 'addLine') {
            let row = table.insertRow(-1);
            let cell1 = row.insertCell(0);
            cell1.className = 'heading';
            for (let i = 1; i < 7; i++) {
                let cell2 = row.insertCell(i);                                
            }  
        } else if (elem.className == 'deleteLine') {
            table.deleteRow(-1);
        }
    })
})