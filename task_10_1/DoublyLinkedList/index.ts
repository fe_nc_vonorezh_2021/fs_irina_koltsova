import { LinkedList } from "./DoublyLinkedList";

const list: LinkedList<number> = new LinkedList();

list.insertInBegin(1);
list.insertAtEnd(2);
list.insertAtEnd(3);
list.insertAtEnd(4);
list.insertAtEnd(5);
console.log(list.toArray());
console.log(list.getElement(3));
list.insertByIndex(8, 2);
console.log(list.toArray());
list.remove(10);
list.edit(4, 7);
console.log(list.toArray());
