const arr: number[] = [...new Array(15)].map(() => Math.round(Math.random() * 100));
console.log(`Изначальный массив: ${arr}`);

const swap = (items: number[], firstIndex: number, secondIndex: number): void => {
    const temp = items[firstIndex];
    items[firstIndex] = items[secondIndex];
    items[secondIndex] = temp;
}

const separation = (items: number[], left: number, right: number): number => {
    let middle = items[Math.floor((right + left) / 2)];
    let i = left;
    let j = right;
    while (i <= j) {
        while (items[i] < middle) {
            i++;
        }
        while (items[j] > middle) {
            j--;
        }
        if (i <= j) {
            swap(items, i, j);
            i++;
            j--;
        }
    }
    return i;
}

const quickSort = (items: number[], left: number, right: number): number[] => {
    let index;
    if (items.length > 1) {
        index = separation(items, left, right);
        if (left < index - 1) {
            quickSort(items, left, index - 1);
        }
        if (index < right) {
            quickSort(items, index, right);
        }
    }
    return items;
}

const result = quickSort(arr, 0, arr.length - 1);
console.log(result); //отсортированный массив