let form = document.querySelector(".form"),
    name = document.querySelector(".name"),
    surname = document.querySelector(".surname"),
    email = document.querySelector(".email"),
    phone = document.querySelector(".phone"),
    message = document.querySelector(".form_text"),
    button = document.querySelector(".form_button");            

//регулярные выражения для номера телефона и email
regexpPhone = /^\+\d\(\d{3}\)\d{2}\-\d{2}\-\d{3}$/;
regexpEmail = /^([0-9A-Z\.]{1,})@([A-Z]{1,}\.)([A-Z]{2,})$/iu;

//заполняем ранее введенные поля при перезагрузке или закрытии страницы
window.onload = function () {
  name.value = localStorage.getItem("name");
  surname.value = localStorage.getItem("surname");
  email.value = localStorage.getItem("email");
  phone.value = localStorage.getItem("phone");
  message.value = localStorage.getItem("message");
};

//проверка email
const emailValidator = (emailElem) => {
  if (regexpEmail.test(emailElem)) {
    return true;    
  } else {
      return false;     
  }
}

//проверка номера телефона
const phoneValidator = (phoneElem) => {
  if (regexpPhone.test(phoneElem) || phoneElem.length == 0) {
    return true;    
  } else {
      return false;
  }    
}

//оформление полей формы при правильно/неправильно введенных значений телефона и email
function checkForm(phoneElem, emailElem) {
  if (phoneValidator(phoneElem) && emailValidator(emailElem)) {		
    phone.style.border = "none";
    email.style.border = "3px solid orange";    
    return true;
  } else if (!phoneValidator(phoneElem) && !emailValidator(emailElem)) {		  
      phone.style.border = "3px solid red";
      email.style.border = "3px solid red";
      return false;
  } else if (!emailValidator(emailElem)){		  
      email.style.border = "3px solid red";
      phone.style.border = "none";
      return false;
  } else { 
      email.style.border = "3px solid orange";
      phone.style.border = "3px solid red";
      return false;
  }	
}

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {
  options = {
    path: '/',    
    ...options
  };
  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }
  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }
  document.cookie = updatedCookie;
}

//при клике на кнопку обрабатываем введенные данные и делаем запись в куки
form.addEventListener("submit", (event) => {
  event.preventDefault();		
  checkForm(phone.value, email.value);       
  if (getCookie("flag")) {
    alert(`${name.value} ${surname.value}, Ваше обращение обрабатывается`);    
    event.target.reset();
  } else if (checkForm(phone.value, email.value)) {
      document.cookie = `${name.value}=${surname.value}=обращение отправлено`;        
      alert(`${name.value} ${surname.value}, спасибо за обращение!`);      
      setCookie("flag", true);            
      event.target.reset();                     
  }
})




 