export interface Person {
    name: string;
    van: number;        
}

export class PersonCart  implements Person{
    constructor(public name: string, public van: number){}

    getName(): string {
        return this.name;
    }

    setName(newName: string): string {
        return this.name = newName;        
    }

    getVan(): number {
        return this.van;
    }

    setVan(newVan: number): number {
        return this.van = newVan;        
    }
}
