import {Person, PersonCart} from "./Person";

interface Conductor extends Person {
    age: number;
    getPass(): void;
    work(): void;
}

export class ConductorCart extends PersonCart implements Conductor {
    constructor(public name: string, public van: number, public age: number) {
        super(name, van);
    }

    getPass(): void {
        console.log(`Кондуктор: ${this.name}, возраст: ${this.age}.`);
    }

    work(): void {
        console.log(`Кондуктор ${this.name} вышел на смену в ${this.van} вагоне.`)
    }    
}
