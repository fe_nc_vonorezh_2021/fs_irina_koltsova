import {PassengersCart} from "./Passengers";
import {ConductorCart} from "./Conductors";


interface Train {
    chief: string;
    driver: string;
    passenger: PassengersCart;
    conductor: ConductorCart;    
}

export class TrainPlan implements Train {    
    constructor(public chief: string, public driver: string, public passenger: PassengersCart, public conductor: ConductorCart){}
    
    setDriver(newDriver: string): void {
        console.log(`Машинист ${this.driver} ушел в отпуск, его заменит ${newDriver}.`);
        this.driver = newDriver;
    }

    setChief(newChief: string): void {
        console.log(`Шеф ${this.chief} уволился. Познакомьтесь, ваш новый шеф ${newChief}.`);
        this.chief = newChief;
    }

    getTrainPlan(): void {
        let today = new Date();
        if (this.passenger.van === this.conductor.van) {
            console.log(`На сегодня ${today} пассажира ${this.passenger.name} обслуживает ${this.conductor.name}.`);
        }        
    }
}

