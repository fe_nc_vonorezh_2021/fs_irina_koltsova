import { ConductorCart } from "./Conductors";
import { PassengersCart } from "./Passengers";
import { TrainPlan } from "./Train";
import { FabricMethod } from "./Fabric";

const factory = new FabricMethod();
const passenger = factory.create(PassengersCart, 'Алиса Шершева', 1, 28);
console.log(passenger);
passenger.getData();

const conductor = factory.create(ConductorCart, 'Виктор Митько', 1, 24);
console.log(conductor);

const train = factory.create(TrainPlan, 'Виталий Хрипушин', 'Андрей Стежко', passenger, conductor);
console.log(train);
train.setDriver('Максим Иванов');
train.getTrainPlan();