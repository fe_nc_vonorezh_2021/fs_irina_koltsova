const log = (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    const originalMethod = descriptor.value;
    descriptor.value = function(...args: any[]){
        console.log(`Object ${args[0].name} created!`);
        return originalMethod.apply(this, args);
    }
}

export class FabricMethod {
    
    @log
    create<T>(type: {new (...args: any[]) : T}, ...args: any[]): T {
        return new type(...args);
    }
}