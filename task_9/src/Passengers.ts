import {Person, PersonCart} from "./Person";

interface Passengers extends Person {
    ticket: number;
    getData(): void;
    hurry(): void;
}

export class PassengersCart  extends PersonCart implements Passengers  {
    constructor(public name: string, public van: number, public ticket: number) {
        super(name, van);
    }    

    getData(): void {
        console.log(`Пассажир ${this.name} поедет в вагоне ${this.van}, его место под номером ${this.ticket}.`);
    }

    hurry(): void {
        console.log(`${this.name} спешит на поезд!`);
    }
}
