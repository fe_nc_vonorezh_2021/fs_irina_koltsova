class Person {	
	constructor({name, van}) {
		this.name = name;		
		this.van = van;
	}

	get Name() {
		return this.name;
	}

	set Name(newName) {
		this.name = newName;
		return this.name;
	}

	get Van() {
		return this.van;
	}	
}

class Passengers extends Person {
	constructor({name, van, ticket}) {
		super({name, van});
		this.ticket = ticket;
	}		
}

class Conductor extends Person {
	constructor({name, van, age}) {
		super({name, van});
		this.age = age;
	}	

	work() {
		console.log(`Проводник ${this.name} в этой поездке работает в ${this.van} вагоне`);
	}
}

class Train {
	constructor({chief, driver, passengers, cond}) {
		this.chief = chief;		
		this.driver = driver;
		this.passengers = passengers;
		this.cond = cond;		
	}

	get Chief() {
		this.chief = chief;
	}

	set Chief(newChief) {
		this.chief = newChief;
		return this.chief;
	}

	get Driver() {
		this.driver = driver;
	}

	set Driver(newDriver) {
		this.driver = newDriver;
		return this.driver;
	}

	calcPass(){
		return this.passengers.length;		
	}
	
	defClass() {
		this.passengers.forEach(pass => {pass.van === '1' ? pass.drive('в купе') : pass.drive('в плацкарте')});
	}

	getVan() {
		this.passengers.forEach(pass => {pass.van === '1' ? pass.drive('в первом вагоне') : pass.drive('во втором вагоне')});
	}

	changeCond(nameCond) {
		console.log(`Кондуктор ${nameCond} теперь работает на другом поезде.`);
		let newCond = this.cond.filter(conductor => conductor.name != nameCond);
		return newCond;
	}
}

const passengers = [
	new Passengers({name: 'Шишкина Анна', van: '1', ticket: '1'}),
	new Passengers({name: 'Васильев Роман', van: '2', ticket: '4'}),
	new Passengers({name: 'Петрова Анастасия', van: '1', ticket: '6'}),
	new Passengers({name: 'Ковалев Михаил', van: '1', ticket: '3'}),
	new Passengers({name: 'Кузнецов Владислав', van: '2', ticket: '1'}),
	new Passengers({name: 'Иванько Елена', van: '2', ticket: '3'}),
	new Passengers({name: 'Шустров Андрей', van: '2', ticket: '2'}),
	new Passengers({name: 'Бочкарева Людмила', van: '1', ticket: '2'})
]

const cond1 = new Conductor({name: 'Маликова Софья', van: '1', age: '20'});
const cond2 = new Conductor({name: 'Волова Александра', van: '1', age: '33'});
const cond3 = new Conductor({name: 'Дронов Антон', van: '2', age: '26'});
const cond4 = new Conductor({name: 'Банькова Евгения', van: '2', age: '19'});

const train = new Train({chief: 'Харин Владимир', driver: 'Никонов Александр', passengers: passengers, cond: [cond1, cond2, cond3, cond4]});

console.log(`Начальник поезда: ${train.chief}`);
console.log(`Машинист поезда: ${train.driver}`);
console.log(`В этой поездке количество пассажиров составило: ${train.calcPass()} человек.`);
console.log('Расположение пассижиров в поезде:')
console.log(train.defClass());
console.log('Номер вагона:');
console.log(train.getVan());
console.log(train.changeCond('Маликова Софья'));