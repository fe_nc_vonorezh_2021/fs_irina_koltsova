let numbers = document.querySelectorAll('.number');
let operations = document.querySelectorAll('.operation');
let point = document.querySelector('.decimal');
let clearing = document.querySelectorAll('.clean');
let display = document.getElementById('display');
let res = document.getElementById('result');
let flag = false;
let pastValue = 0; //предыдущее число
let pastOperation = ''; //предыдущая операция
let history = '';

//функция добавления выбранного числа
//здесь реализована стрелочная функция
const numberBtn = (num) => {			
  if (flag) {
    display.value = num;		
    flag = false;
  } else {
      display.value += num;				
  }
  history += num;				
}

//функция, определяющая операции
//при выборе операции "квадрат" или "степень числа" необходимо: выбрать число, нажать на нужную функцию, нажать равно (=)
function operationCalc(op) {
  let newValue = display.value;	
  if (flag && op === '=') {
    if (pastOperation === 'xⁿ') {
      pastValue = parseFloat(newValue*newValue);
    } else if (pastOperation === '√') {
        pastValue = parseFloat(Math.sqrt(newValue));
    } else {
        display.value = pastValue;
    }					
  } else {
      flag = true;
      if (pastOperation === '+') {
        pastValue += parseFloat(newValue);			
      } else if (pastOperation === '-') {
          pastValue -= parseFloat(newValue);
      } else if (pastOperation === 'x') {
          pastValue *= parseFloat(newValue);
      } else if (pastOperation === '÷') {
          pastValue /= parseFloat(newValue);		
      } else {
          pastValue = parseFloat(newValue);
      }						
  }		
  display.value = pastValue;
  pastOperation = op;
  if (op === '=') {
    history += op + pastValue;
  } else {
      history += pastOperation;
  }	
  res.innerHTML = history;
}

//реализация десятичной части
const decimal = () => {
  let dec = display.value;
  if (flag) { 
    dec = '0.'; //при отсутствии числа клик на "." даст "0."
    history += '0.';
    flag = false;
  } else {
      if (dec.indexOf('.') === -1) {
        dec += '.';
        history += '.';
      }
  }
  display.value = dec;
  res.innerHTML = history;
}

//очистка экрана - С, удаление последней операции - СЕ
//пример выполнения СЕ: число -> "-" -> число -> СЕ -> "=" -> (получаем первое число) "+" -> число -> "=" -> (результат сложения)
function clearDisplay(id) {
  if (id === 'c') {
    display.value = '0';
    pastValue = 0;
    pastOperation = '';
    flag = true;
    history = '';
    res.innerHTML = '';
  } else {
      display.value = '';
      flag = true;
      history = '';
      res.innerHTML = '';
  }
}

for (let i = 0; i < numbers.length; i++) {
  let numBtn = numbers[i];
  numBtn.addEventListener('click', function (e) {
    numberBtn(e.target.textContent);
  });
}

for (let i = 0; i < operations.length; i++) {
  let opBtn = operations[i];
  opBtn.addEventListener('click', function (e) {
    operationCalc(e.target.textContent);
  })
}

point.addEventListener('click', function (e) {
  decimal(point.innerHTML);
})

for (let i = 0; i < clearing.length; i++) {
  let clearBtn = clearing[i];
  clearBtn.addEventListener('click', function (e) {
    clearDisplay(e.target.id);
  })
}

